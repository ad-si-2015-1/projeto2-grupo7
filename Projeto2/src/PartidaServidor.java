import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author rychard
 *
 */

public class PartidaServidor {
	//Endereco IP do servidor, fixo
	private InetAddress ipServidor;
	//indica a porta usada
	private int porta;
	private ServerSocket servidor;
	// jogadores da partida
	private Jogador[] jogadores;
	// baralho da partida
	private Baralho baralho;
	// campeao da partida
	private Jogador campeao;
	// marcador fim de jogo
	private Boolean partidaFinalizada;
	//numero de jogadores que pararam e esperam por resultado
	private int jogadoresParados;

	public PartidaServidor(int porta){

		try {
			this.porta = porta;
			this.servidor = new ServerSocket(this.getPorta());
			this.baralho = new Baralho(1,true);
			this.partidaFinalizada = false;
			this.jogadores = new Jogador[2];
		} catch (IOException e) {
			System.out.println("Erro ao iniciar o servidor");
		}

	}

	public PartidaServidor(Jogador[] jogadores, int porta) throws IOException {
		this.jogadores = jogadores;
		this.porta = porta;
		this.servidor = new ServerSocket(this.getPorta());

		if(jogadores.length < 2){
			//valida��ao numero de jogadores

			// A CRIAR 
		}
		else{		
			this.baralho = new Baralho(1,true);
			this.partidaFinalizada = false;
			iniciar();
		}

	}

	public void iniciar(){

		while(true){

			System.out.println("Aguardando conexoes....");

			try {

				if (jogadores[0] == null) {

					jogadores[0] = new Jogador();

					jogadores[0].setConexao(servidor.accept());
					enviarMsg(jogadores[0].getConexao(), "Conexao aceita, bem vindo ao jogo!Qual o seu nome?");
					jogadores[0].setNome(lerMsg(jogadores[0].getConexao()));
					enviarMsg(jogadores[0].getConexao(), "Aguardando demais jogadores, aguarde " + jogadores[0].getNome());
				}

				else if(jogadores[1] == null){

					jogadores[1] = new Jogador();

					jogadores[1].setConexao(servidor.accept());
					enviarMsg(jogadores[1].getConexao(), "Conexao aceita, bem vindo ao jogo!Qual o seu nome?");
					jogadores[1].setNome(lerMsg(jogadores[1].getConexao()));

					//Comunica a todos que o jogo vai comecar!
					enviarMsg("O jogo vai comecar em...");
					Thread.sleep(1000);
					enviarMsg("3");
					Thread.sleep(1000);
					enviarMsg("2");
					Thread.sleep(1000);
					enviarMsg("1");
					Thread.sleep(1000);
					this.distribuiCartas();	
					
					fecharConexaoJogadores();
					
				}

				else{
					Socket s = servidor.accept();
					enviarMsg(s, "Conexao nao aceita, ja existem jogadores suficientes! Por favor aguarde a proxima rodada");
					s.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro ao tentar conexcao com o Jogador");
			}
		}
	}

	//metodo que distribui cartas para os jogadores e verifica o ganhador
	public void distribuiCartas(){

		//contador de cartas/rodadas
		int k = 0;

		while(!partidaFinalizada && k <= 51){

			for(int i = 0; i < this.jogadores.length; i++){

				// a primeira rodada, apenas distribui uma carta a cada jogador
				if(k < jogadores.length){
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					enviarMsg(jogadores[i].getConexao(), "Sua primeira carta e: " + jogadores[i].getMinhasCartas()[0]);
					k++;
				}
				
				//Se todos decidiram parar para receber o resultado no final do jogo, então o campeao e informado
				else if(this.jogadoresParados == jogadores.length){
					
					campeao = new Jogador();
					campeao.setSomaCartas(0);
					
					for(Jogador j : jogadores){	
						campeao =  campeao.getSomaCartas() <= j.getSomaCartas() && j.getSomaCartas() <= 21 ? j : campeao; 						
					}
					
					this.partidaFinalizada = true;
					i = this.jogadores.length;
					informaCampeao();
					
				}

				else{
					// a partir da segunda rodada, conferir se algum jogador esta com 21
					for(int j = 0; j < this.jogadores.length; j++){
						if(this.jogadores[j].getSomaCartas() == 21){
							this.campeao = this.jogadores[j];
							this.partidaFinalizada = true;
							j = this.jogadores.length;
						}
						
						//verifica se algum jogador estourou (passou de 21), nesse caso ele ja e um perdedor (esta fora da partida)
						else if(this.jogadores[j].getSomaCartas() > 21){
							enviarMsg(jogadores[j].getConexao(), "\n\n" + jogadores[j].getNome() + ", voce estourou a quantidade de pontos, voce esta fora!!!");
							campeao = j == 0 ? jogadores[1] : jogadores[0];
							this.partidaFinalizada = true;
							j = this.jogadores.length;
						}
					}

					if(!partidaFinalizada && !jogadores[i].getParouDeJogar()){

						menu(jogadores[i]);
						enviarMsg(jogadores[i].getConexao(), "\n\nAguarde enquanto os outros jogadores fazem sua jogada");
					}
					//Mostra o vencedor do jogo
					else if(partidaFinalizada){
						i = this.jogadores.length;
						informaCampeao();
					}
				}
			}
		}

	}
	
	/**
	 * Metodo que envia mensagens informando o vencedor do jogo.
	 * */
	public void informaCampeao(){
		
		this.enviarMsg(campeao.getConexao(), "Parabens " + campeao.getNome() + ", voce ganhouuuuu! Total de pontos: " + campeao.getSomaCartas());
		this.enviarMsg("O jogador(a) " + campeao.getNome() + " ganhou o jogo!!! Pontos do camepao: " + campeao.getSomaCartas());
		
	}
	
	public void fecharConexaoJogadores(){
		
		enviarMsg("Sua conexao sera fechada!!!");
		
		for(Jogador j : jogadores){
			try {
				Thread.sleep(1000);
				j.getConexao().close();
				j = null;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Erro ao fechar conexao");
			}
		}
		
	}

	public void menu(Jogador jogador){

		boolean loop = true;

		String opcoes = "Digite uma das opcoes!!\r\n\r\n"
				+ "1-Pegar Carta\r\n"
				+ "2-Parar e esperar ate o final com minha pontuacao\r\n"
				+ "3-Ver minha pontuacao\r\n"
				+ "4-Listar minhas cartas\r\n\r\n"
				+ "Digite o numero da opcao escolhida: ";

		while (loop) {

			enviarMsg(jogador.getConexao(), opcoes);
			String ops = lerMsg(jogador.getConexao());
			int op = Integer.parseInt(ops);

			switch (op) {
			case 1:
				this.addCartas(jogador);
				loop = false;
				break;
			case 2:
				jogador.setParouDeJogar(true);
				this.jogadoresParados++;
				loop = false;
				break;
			case 3:
				enviarMsg(jogador.getConexao(), "Sua pontuacao e: " + jogador.getSomaCartas() + "\n\n");
				break;
			case 4:
				enviarMsg(jogador.getConexao(), listarCartas(jogador) + "\n\n");
				break;
			default:
				enviarMsg(jogador.getConexao(), "Opcao invalida, digite uma correta" + "\n\n");
				break;
			}
		}
	}
	
	public void addCartas(Jogador jogador){
		
		jogador.addCartas(baralho.darProximaCarta());
		enviarMsg(jogador.getConexao(), listarCartas(jogador));
		enviarMsg(jogador.getConexao(), "Agora voce tem: " + jogador.getSomaCartas() + " pontos");
		
	}
	
	public String listarCartas(Jogador jogador){
		
		String mensagem = "\nLista de cartas\n";
		
		for(Cartas carta : jogador.getMinhasCartas()){
			if(carta != null)
				mensagem += carta.toString() + "\n";
			
		}
		
		return mensagem;
		
	}

	public String lerMsg(Socket clienteSocket){

		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));

			return reader.readLine();
		} catch (Exception e) {
			System.out.println("Erro na leitura da mensagem");
			return null;
		}
	}

	public Boolean enviarMsg(Socket clienteSocket, String msg){

		String msgEnvio = "\n";
		msgEnvio += msg;
		DataOutputStream saida;

		try {

			saida = new DataOutputStream(clienteSocket.getOutputStream());
			saida.writeBytes(msgEnvio);

		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	/**
	 * Metodo que envia a mensagem para todos os jogadores
	 */
	public Boolean enviarMsg(String msg){

		String msgEnvio = "\nMensagem para todos os jogadores: ";
		msgEnvio += msg;
		DataOutputStream saida = null;

		try {

			for(Jogador jogador : jogadores){

				saida = new DataOutputStream(jogador.getConexao().getOutputStream());
				saida.writeBytes(msgEnvio);
			}

		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}

	public ServerSocket getServidor() {
		return servidor;
	}

	public void setServidor(ServerSocket servidor) {
		this.servidor = servidor;
	}

	public int getJogadoresParados() {
		return jogadoresParados;
	}

	public void setJogadoresParados(int jogadoresParados) {
		this.jogadoresParados = jogadoresParados;
	}

}
